const config = require("@wordpress/scripts/config/webpack.config");

module.exports = {
	...config,
	entry: {
		...config.entry,
		gutenberg_core_enhancements: "./js/index.js",
	},
	output: {
		...config.output,
		path: `${__dirname}/dist`,
		filename: "[name].js",
	},
};
