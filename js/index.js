import { registerBlockStyle, registerBlockVariation } from "@wordpress/blocks";

const __ = Drupal.t;

// * Columns
registerBlockVariation("core/columns", {
	name: "four-columns-25",
	title: "25 / 25 / 25 / 25",
	description: "Four columns; each column 25% wide.",
	icon: (
		<svg
			width="48"
			height="48"
			viewBox="0 0 48 48"
			xmlns="http://www.w3.org/2000/svg"
			aria-hidden="true"
			focusable="false"
		>
			<path
				fill-rule="nonzero"
				d="M39 12a2 2 0 011.995 1.85L41 14v20a2 2 0 01-1.85 1.995L39 36H9a2 2 0 01-1.995-1.85L7 34V14a2 2 0 011.85-1.995L9 12h30zm-24 2H9v20h6V14zm8 0h-6v20h6V14zm2 0v20h6V14h-6zm8 20h6V14h-6v20z"
			/>
		</svg>
	),
	scope: ["block"],
	attributes: {
		className: "has-4-columns",
	},
	innerBlocks: [
		["core/column", { className: "is-25-column" }],
		["core/column", { className: "is-25-column" }],
		["core/column", { className: "is-25-column" }],
		["core/column", { className: "is-25-column" }],
	],
});

// * Table
wp.hooks.addFilter(
	"blocks.registerBlockType",
	"gutenberg_core_enhancements/enforce_table_header",
	(settings, name) => {
		if (name === "core/table") {
			const originalEdit = settings.edit;

			settings.edit = (props) => {
				const { isSelected, attributes, setAttributes } = props;
				const { body, head, isTableHeaderInitialized } = attributes;

				if (
					isSelected &&
					body.length > 0 &&
					(!head || head.length === 0) &&
					!isTableHeaderInitialized
				) {
					// Initialize header only if needed
					const numberOfColumns = body[0].cells.length;
					const newHead = [
						{ cells: Array(numberOfColumns).fill({ tag: "th" }) },
					];
					setAttributes({ head: newHead, isTableHeaderInitialized: true });
				}

				return originalEdit(props);
			};

			settings.attributes = {
				...settings.attributes,
				isTableHeaderInitialized: {
					type: "boolean",
					default: false,
				},
			};

			return settings;
		}

		return settings;
	},
);

// * Button
registerBlockStyle("core/button", {
	name: "no-radius",
	label: __("No Radius"),
	isDefault: true,
});
