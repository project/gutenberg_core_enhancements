# Gutenberg Core Enhancements

A general purpose Gutenberg core block enhancements and extensions, currently does the following:

- Enables **Header section** by default in the `core/table` block.
- Adds **4 columns** layout in the `core/columns` selection by default.
- Adds a **No Radius** style to the Buttons style list in `core/button` by default.
